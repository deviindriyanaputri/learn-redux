import { useDebugValue } from 'react';
import {createStore} from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";


const counterReducer = (state = {counter:0}, action) => {
    if(action.type === 'increment'){
        return{
            counter: state.counter + 1,
        }
    }

    if(action.type === 'increaseByUserInput'){
        return{
            counter: state.counter + action.value,
        }
    }

    if (action.type === 'decrement'){
        return {
            counter: state.counter - 1,
        }
    }
    return state
}
const store = createStore(counterReducer, composeWithDevTools());


export default store;
