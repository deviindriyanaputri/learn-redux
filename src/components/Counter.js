import {useSelector, useDispatch} from 'react-redux';
import classes from './Counter.module.css';

const Counter = () => {
  // const [counter, setSelector] = useState(0)
  const dispatch = useDispatch();
  const counter = useSelector(state => state.counter);

  const incrementNumber = () => {
    dispatch({type: 'increment'});
    
  }

  const increaseByUserInput = () => {
    dispatch({type: 'increaseByUserInput', value:10});
    
  }
  const decrementNumber = () => {
    dispatch({type: 'decrement'});
  }

  const toggleCounterHandler = () => {};

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>{counter}</div>
      <div>
        <button onClick={incrementNumber}>Increment</button>
        <button onClick={increaseByUserInput}>Increase by 5</button>
        <button onClick={decrementNumber}>Decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
